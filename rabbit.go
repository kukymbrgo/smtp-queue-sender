package main

import (
	"github.com/streadway/amqp"
	"gitlab.com/kukymbrgo/golog"
	"strconv"
	"time"
)

// Open RabbitMQ connection
func OpenRabbitMqConn(conf *RabbitConf) (*amqp.Connection, error) {

	var conn *amqp.Connection
	var err error
	var tries uint64 = 0

	connStr := conf.ToConnStr(true)
	connInfo := conf.ToConnStr(false)

	for {
		tries += 1
		golog.Info(
			"trying to connect to RabbitMQ (" + connInfo + "), " +
				strconv.FormatUint(tries, 10) + " attempt")
		conn, err = amqp.Dial(connStr)
		if err == nil {
			golog.Info("connected to RabbitMQ (" + connInfo + ", queue: " + conf.Queue + ")")
			break
		}
		if tries > conf.Attempts {
			return nil, err
		} else {
			golog.Warn(
				"failed to connect to RabbitMQ, waiting " + strconv.FormatUint(conf.Wait, 10) +
					" second(s)")
			time.Sleep(time.Duration(conf.Wait) * time.Second)
		}
	}

	return conn, nil
}

// Prepare queue and consume
func PrepareAndConsume(ch *amqp.Channel, conf *RabbitConf) <-chan amqp.Delivery {
	q, err := ch.QueueDeclare(
		conf.Queue,
		true,
		false,
		false,
		false,
		nil,
	)
	HandleErr(err, "Failed to declare a queue")

	messages, err := ch.Consume(
		q.Name,
		"",
		false,
		false,
		false,
		false,
		nil,
	)
	HandleErr(err, "Failed to connect to RabbitMQ")

	return messages
}
