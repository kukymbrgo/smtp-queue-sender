package main

import (
	"github.com/go-gomail/gomail"
	"gitlab.com/kukymbrgo/golog"
	"strconv"
)

// Mail sender factory
func NewMailSender(conf *SmtpConf, testMode bool) *MailSender {
	s := new(MailSender)
	s.conf = conf
	s.testMode = testMode
	s.dialer = nil
	return s
}

// SMTP Sender
type MailSender struct {
	conf     *SmtpConf
	dialer   *gomail.Dialer
	testMode bool
}

// Send mail
func (s *MailSender) SendMail(mail *MailItem) error {
	golog.Info("trying to send mail " + mail.Id + " to " + mail.RecipientEmail)

	if s.testMode {
		golog.Info("test mode is on, send mail imitation")
		return nil
	}

	m := s.mailToMessage(mail)
	d := s.getDialer()

	err := d.DialAndSend(m)

	s.mailSendInfo(mail, err)

	return err
}

// Convert MailItem to gomail.Message
func (s *MailSender) mailToMessage(mail *MailItem) *gomail.Message {
	var mime string
	if mail.IsHtml {
		mime = "text/html"
	} else {
		mime = "text/plain"
	}

	m := gomail.NewMessage()
	m.SetHeader("From", m.FormatAddress(s.conf.SenderEmail, s.conf.SenderName))
	m.SetHeader("To", m.FormatAddress(mail.RecipientEmail, mail.RecipientName))
	m.SetHeader("Subject", mail.Subject)
	m.SetBody(mime, mail.Content)

	if mail.Unsubscribe != "" {
		m.SetHeader("List-Unsubscribe", "<"+mail.Unsubscribe+">")
	}

	return m
}

// Get dialer instance
func (s *MailSender) getDialer() *gomail.Dialer {
	if s.dialer == nil {
		host := s.conf.Host
		if s.conf.Port > 0 {
			host += ":" + strconv.FormatUint(uint64(s.conf.Port), 10)
		}
		s.dialer = gomail.NewDialer(s.conf.Host, s.conf.Port, s.conf.User, s.conf.Password)
	}

	return s.dialer
}

// Log mail send result info
func (s *MailSender) mailSendInfo(mail *MailItem, err error) {
	if err == nil {
		golog.Info("mail #" + mail.Id + " was successfully sent")
	} else {
		golog.Warn("failed to send mail #"+mail.Id, err)
	}
}
