package main

import (
	"github.com/json-iterator/go"
)

// MailItem factory from JSON
func NewMailItem(json []byte) (*MailItem, error) {
	m := new(MailItem)
	err := jsoniter.Unmarshal(json, m)
	if err != nil {
		return nil, err
	}

	return m, nil
}

// MailItem
type MailItem struct {
	Id             string `json:"id"`
	RecipientEmail string `json:"recipient_email"`
	RecipientName  string `json:"recipient_name"`
	Subject        string `json:"subject"`
	Content        string `json:"content"`
	IsHtml         bool   `json:"is_html"`
	Unsubscribe    string `json:"unsubscribe"`
}
