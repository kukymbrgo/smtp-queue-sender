package main

import (
	"flag"
	"gitlab.com/kukymbrgo/golog"
	"os"
)

func main() {

	appParams := new(appParams)
	appParams.read()

	appConf := NewAppConf()
	if appParams.ConfigPath != "" {
		appConf.ReadFromJsonFile(appParams.ConfigPath)
	}

	conn, err := OpenRabbitMqConn(appConf.Rabbit)
	HandleErr(err, "Failed to open connection")
	defer conn.Close()

	ch, err := conn.Channel()
	HandleErr(err, "Failed to open a channel")
	defer ch.Close()

	forever := make(chan bool)

	messages := PrepareAndConsume(ch, appConf.Rabbit)
	sender := NewMailSender(appConf.Smtp, appParams.TestMode)

	go func() {
		for msg := range messages {
			mail, err := NewMailItem(msg.Body)
			if err == nil {
				sender.SendMail(mail)
			} else {
				golog.Warn("failed to read message to mail item", err)
			}
			msg.Ack(false)
		}
	}()

	golog.Info("waiting for messages")

	<-forever
}

// App params structure
type appParams struct {
	ConfigPath string
	TestMode   bool
}

// Init App params
func (p *appParams) read() *appParams {
	isHelp := flag.Bool("help", false, "Show help")
	confPath := flag.String("c", "", "Config JSON file path")
	isTest := flag.Bool("test", false, "Test mode (don't send mails)")

	flag.Parse()

	if *isHelp {
		flag.PrintDefaults()
		os.Exit(0)
	}

	p.ConfigPath = *confPath
	p.TestMode = *isTest

	return p
}

// Fatal on non-empty error
func HandleErr(err error, msg string) {
	if err != nil {
		golog.Err(msg, err)
	}
}
