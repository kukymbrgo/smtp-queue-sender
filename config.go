package main

import (
	"errors"
	"github.com/json-iterator/go"
	"io/ioutil"
	"os"
	"strconv"
)

// Create new AppConf instance with default values
func NewAppConf() *AppConf {
	conf := new(AppConf)
	conf.ReadFromJsonFile("./config/default.json")

	return conf
}

// App config
type AppConf struct {
	Rabbit *RabbitConf `json:"rabbit"`
	Smtp   *SmtpConf   `json:"smtp"`
}

// Read config from JSON file
func (c *AppConf) ReadFromJsonFile(path string) *AppConf {
	json := readFile(path)
	err := jsoniter.Unmarshal(json, c)
	if err != nil {
		HandleErr(err, "Failed to read config JSON")
	}

	return c
}

// RabbitMQ config
type RabbitConf struct {
	User     string `json:"user"`
	Password string `json:"password"`
	Host     string `json:"host"`
	Port     int    `json:"port"`
	Attempts uint64 `json:"attempts"`
	Wait     uint64 `json:"wait"`
	Queue    string `json:"queue"`
}

// Get as a rabbit mq connection string
func (c *RabbitConf) ToConnStr(addPassword bool) string {
	res := "amqp://"
	res += c.User
	if addPassword {
		res += ":" + c.Password
	}
	res += "@" + c.Host + ":" + strconv.FormatUint(uint64(c.Port), 10)

	return res
}

// SMTP sender config
type SmtpConf struct {
	Host        string `json:"host"`
	Port        int    `json:"port"`
	User        string `json:"user"`
	Password    string `json:"password"`
	SenderEmail string `json:"sender_email"`
	SenderName  string `json:"sender_name"`
}

// Read file to bytes slice
func readFile(path string) []byte {
	stat, err := os.Stat(path)
	if err != nil {
		HandleErr(err, "Failed to read config file")
	}

	if stat.IsDir() {
		HandleErr(errors.New(path+" is a directory"), "Failed to read config file")
	}

	file, err := os.Open(path)
	if err != nil {
		HandleErr(err, "Failed to read config file")
	}
	defer file.Close()

	bytes, err := ioutil.ReadAll(file)
	if err != nil {
		HandleErr(err, "Failed to read config file")
	}

	return bytes
}
