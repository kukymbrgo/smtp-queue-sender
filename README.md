# SMTP Queue Mails Sender

App to send mails received from RabbitMQ.

## Installation

### Docker

Run `docker-compose up -d --build`.

### Build

    go get -d -v ./... && go install -v ./...

## Execution

To execute the app, run `./smtp-queue-sender`. Available options:
* `-c "path/to/config.json"` — path to JSON-encoded config file to override default options. See `config/default.json` for default options values.
* `-test` — enable "test mode". When test mode is on, app does not send any emails.
* `-help` — show help

## Usage

* Declare Queue in RabbitMQ with next params:
    * `name` — value from the config file (default or custom), `rabbit.queue`
    * `durable=true`
* Send messages with JSON-encoded mail data to RabbitMQ. See `example/message.json` for message fields.
